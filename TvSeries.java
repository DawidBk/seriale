package TvSeries;

import java.util.List;

public class TvSeries {

	private long id;
	private String name;
	
	List<Season>seasons;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

