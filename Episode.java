package TvSeries;

import java.time.Duration;
import java.time.LocalDate;

public class Episode {

	private String name;
	private LocalDate releaseDate;
	private int episodeNumber;
	private Duration duration;
	
	public Episode() {}
	
	public Episode(String name, LocalDate releaseDate, int episodeNumber, Duration duration) {
		super();
		this.name = name;
		this.releaseDate = releaseDate;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getEpisodeNumber() {
		return episodeNumber;
	}
	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	
}
