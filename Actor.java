package TvSeries;

import java.time.LocalDate;

public class Actor {

	//private long id;
	
	private String name;
	private LocalDate dateOfBirth;
	private String biography;
	
	public Actor() {}
	
	public Actor(String name, LocalDate dateOfBirth, String biography) {
		super();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}
	
	//public long getId() {
	//	return id;
	//}
	//public void setId(long id) {
	//	this.id = id;
	//}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
}
	
	
	

