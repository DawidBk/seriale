package TvSeries;

import java.time.LocalDate;
import java.util.List;

public class Season {
	private int seasonNumber;
	private int yearOfRelease;
	List<Episode>episodes;
	
	public Season() {}
	
	public Season(int seasonNumber, int yearOfRelease) {
		super();
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRelease;
	}

	
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	
	
	
}
